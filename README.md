# Pathfinder

## Description
Pathfinder is a moodle block plugin, which aims at creating multiple learning pathes inside a moodle course based on the learning types of the students. The learning types are based on the Felder-Silverman Model (Felder, Richard & Silverman, L.K.. (2002). Learning and teaching styles in engineering education. Engr. Education. 78. 674-681).

## Table of contents
- [Installation](#installation)
- [Usage](#usage)
- [Classification of Learning Materials](#classification)

## Installation
To install the plugin, navigate to your moodle instance and login with an account, that has administrative permissions.
Go to Website Administration -> Install plugin and install the plugin from pathfinder.zip.

## Usage
1. To use the plugin for a course, first make sure you have all the learning materials and activities needed inside your moodle course. 
2. Make sure to include the needed Classification Tests for the students. There are 4 Classification Tests needed for each of the corresponding learning style dimension of the Felder-Silverman Model: Processing, Perception, Input and Understanding. You can upload the question with the xml file "Questions_all_dimensions" in the question bank on Moodle. In the next steps, you need to create four Moodle Tests for each of the dimension and add the questions by filtering with the tagg of the corresponding dimension.
3. Enable the Edit mode and add a Pathfinder block in the block section. 
4. Specify the global pathfinder settings in Pathfinder Settings. After that, you can configure the current course in Pathfinder Configuration.

## Classification of Learning Materials and Activities
There are in total 19 leraning material types defined in the plugin. Each of them are mapped to one of the for dimensions od the Felder-Silverman Model. Please note that the classification inside the dimension is simplified to the three ranks: Processing (active, both, reflective); Perception(sensing, both, intuitive); Input(verbal, both, visual); Understanding(global, both, sequential). You can look up the mapping in the file "Classification of Learning Materials and Activities".